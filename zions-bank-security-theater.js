// ==UserScript==
// @name       Zions Bank Security Theater Destroyer
// @namespace  https://banking.zionsbank.com/
// @version    0.1
// @description  Disable the annoying security theater on Zions Bank's online banking page.
// @match      https://banking.zionsbank.com/*
// @copyright  2014 jmnet.us - mit license
// ==/UserScript==

/* 
 * Copyright (C) 2014 jmnet.us
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


if(document.onmousedown) {
    console.log( "\n _[TT]_j__,(  j\"n,.\n(_)oooo(_)'  ,m^.Y.v\n\nDemolishing security theater, please wait..." );
    
    document.oncontextmenu = function(event) {
        try {
            console.log( "Letting right click through, because I care not for your security." );
            return true;
        } catch(e) {
            // something went mysteriously wrong, but we'll return true anyway
            return true;
        }
    };
    
    document.onmousedown = function(event) {
        try {
            if (event.ctrlKey && event.which == 1) {
                if (!((event.target instanceof HTMLSelectElement) || (event.target instanceof HTMLOptionElement))) {
                    console.log( "Letting ctrl-click through, because I care not for your security." );
                    return true;
                }
            }
        } catch(e) {
            // something went mysteriously wrong, but we'll return true anyway
            return true;
        }
    };
}
